package com.userregistration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserRegistrationBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserRegistrationBackEndApplication.class, args);
    }

}
